---
layout: post
title:  "Static site from scratch"
date:   2022-04-10 15:32:14 -0300
categories: jekyll update
description: "... when you're totally out of your depth and have no idea what you're doing. Pointers on making a sustainable and unintrusive blog using free software and no knowledge. Bordering on a tutorial."
thumbnail: "/images-html/thumbnails/posts-tech/how-to-make-static-site-gitlab.png"
trash: "no"
draft: "no"
---

The goal setting up this blog was for it to require as little long-term maintenance as possible, so that if I had big stretches away, I could quickly pick it back up again. I wanted to avoid nasty surprises, like disappearing assets and functionality, waking up one day to find it's just gone, etc. And I figured the easiest way to reduce maintenance was to have zero expenses -- *can't forget to pay for a server I don't pay for* -- and I could minimize nasty surprises by using zero commercial platforms: brogrammers can't finesse me if I flee.

Bit of an extreme approach, but it has several advantages. Most obvious one: no pressure to generate revenue. I don't need ads or trackers, so I can provide a nice, snappy, clean reading experience without popups, SEO or scroll-increasing BS. I can talk like a human about anything I choose, whenever I want[^1]. I could even make a cooking blog that ain't hot garb-- oh, that's {% include post-link.html link-text="already a thing." link-url="https://justthedarnrecipe.com/" %} Nice!

#### Summarily, here's the four choices I made going in:

- ***Platform*** &rarr; {% include post-link.html link-text="**Gitlab pages**" link-url="https://docs.gitlab.com/ee/user/project/pages/" %}
    - <div class="lst-sml-div"><em>Usually blog platforms allow for either free hosting with ads enabled or adless paid hosting. Best of both worlds only seems to be provided by Github and Gitlab with their Pages programs. I found {% include post-link.html link-text="this post" link-url="https://vincenttam.gitlab.io/post/2018-09-16-staticman-powered-gitlab-pages/0/" %} after having already chosen the fox over the cat, but it lists the same reasons that I had.</em></div>
<div style="display:block; margin-bottom: -15px;"></div>
<!--more-->
- ***Static site generator*** &rarr; {% include post-link.html link-text="**Jekyll**" link-url="https://docs.gitlab.com/ee/user/project/pages/" %}
    - <div class="lst-sml-div"><em>No earth-shattering reasons for this choice. I could have gone for Harp, Hexo, Hila, Hugo, any other 4-letter SSG beginning with 'H'. I chose Jekyll because I used it before and it seemed nice.</em></div>
<div style="display:block; margin-bottom: -15px;"></div>
- ***Comments*** &rarr; {% include post-link.html link-text="**Staticman**" link-url="https://staticman.net" %}
    - <div class="lst-sml-div"><em>I looked up {% include post-link.html link-text="this list" link-url="https://averagelinuxuser.com/static-website-commenting/" %} of comment systems for static sites and most free plans seemed either limiting or intrusive. Staticman is free and requires no ads or shady stuff like trackers. Only caveat is that it takes a while for comments to be posted.</em></div>
<div style="display:block; margin-bottom: -15px;"></div>
- ***Javascript*** &rarr; {% include post-link.html link-text="**No**" link-url="https://en.wiktionary.org/wiki/no" %}

So with these tenets set, I was ready to get working. Just one problem:

<div style="display:block; margin-bottom: -10px;"></div>
<p style="text-align: center; color: #333333;"><em>I had absolutely <b>no idea</b> what I was doing.</em></p>

<div style="display:block; margin-bottom: 45px;"></div>
This isn't *really* a tutorial on how to build a static site with Gitlab. There's a glut of those: aside from each platform's own documentation, there are {% include post-link.html link-text="plenty" link-url="https://blog.ruanbekker.com/blog/2018/12/20/setup-a-gitlab-runner-on-your-own-server-to-run-your-jobs-that-gets-triggered-from-gitlab-ci/" %} of {% include post-link.html link-text="excellent instructions" link-url="https://sayzlim.net/create-jekyll-site-beginners/" %} for {% include post-link.html link-text="this very thing," link-url="https://mademistakes.com/mastering-jekyll/static-comments/" %} which I myself followed. Beyond that the development process is same as always: get things wrong over and over 'til the increasingly mysterious error messages lead to the correct forum post. Not much I can help with there.

However, likely because I had no idea what I was doing, there were episodes where I'd get stumped on the weirdest hurdles: things either too obvious or specific to show up in any regular tutorial. On those occasions I wished there were something beyond a tutorial, designed for people totally out of their depth: a *threetorial*[^2], if you will. This is such a threetorial. It focuses on those episodes, and on what the solutions were after all, so that you don't have to waste your time like I did. I'll also intersperse some opinions so that you have to waste your time anyway. So...
<div style="display:block; margin-bottom: 35px;"></div>

#### ***Hurdle #0*** &rarr; Where / How to even start?

One of the scariest prospects in any long-term project is committing to some decision only to find out much later that it was the wrong one, and that all the work done on top of that decision has to be scrapped. Every failure's a lesson, but not every lesson's insightful: months into working on **X**, you wanna learn a bit more than just *"don't do **X**"*.

Better make sure the foundation for your project is solid, then. To start, you want to ***decide on a workflow.*** Ideally, it should be sufficiently snappy to let you solve problems through trial and error: if it's too laborious to go from making some small change to seeing its result, you'll lose motivation. I dunno what's the platonic form of a static site workflow, but I think mine's pretty good, and likely fairly close to the standard one.

#### Here's how my workflow looks:

1. <div style="margin-bottom: 4px;">There's <b id="boldWK">two versions</b> of the website:</div>
    * This <span id="birdLK">***online***</span>[^3] version &rarr; <span id="birdTX">`https://suneggv.gitlab.io/suneggv`</span>
    * My <span id="songLK">***local***</span> version &rarr; <span id="songTX">`http://127.0.0.1:4000/suneggv`</span>
    <div style="display:block; margin-bottom: 10px;"></div>
2. <div style="margin-bottom: 10px;">I <b id="boldWK">work on the website</b> with the <span id="songLK"><b>local</b></span> version:</div>
    * To start, I run Jekyll locally by doing <code class="code" id="term">$ bundle exec jekyll serve</code> on the root folder of the website's <span id="songLK">**local**</span> version (*that's the one containing the `.bundle` folder*). This generates the website at <span id="songTX">`http://127.0.0.1:4000/suneggv`</span>.
    <div style="display:block; margin-bottom: 6px;"></div>
    * Also to start, in another terminal instance I do <code class="code" id="term">$ sudo gitlab-runner status</code>[^sudo], and if the response isn't `gitlab-runner: Service is running`, then I do <code class="code" id="term">$ sudo gitlab-runner start</code>. This is important for step 4 below.
    <div style="display:block; margin-bottom: 3px;"></div>
    * Then I have at least the following 7 windows[^4] open:
        - <div id="boldWK" style="margin-top:10px;"><em><b>Workspace 1 (main one):</b></em></div>
            * <span id="lstSml">Text editor;</span>
            <div style="margin-bottom: -3px;"></div>
            * <span id="lstSml">Browser with <span id="songTX">`http://127.0.0.1:4000/suneggv`</span> open;</span>
            <div style="margin-bottom: -6px;"></div>
            * <span id="lstSml">Terminal window running <code class="code" id="term">$ htop</code>[^5].</span>
        <div style="margin-bottom: 5px;"></div>
        - <span id="boldWK">***Workspace 2 (secondary):***</span>
            * <span id="lstSml">File manager;</span>
            <div style="margin-bottom: -3px;"></div>
            * <span id="lstSml">Terminal window running <code class="code" id="term">$ bundle exec jekyll serve</code>;</span>
            <div style="margin-bottom: -3px;"></div>
            * <span id="lstSml">Idle terminal window *(for running <code class="code" id="term">$ git</code> from the <span id="songLK">**local**</span> version root folder)*;</span>
            <div style="margin-bottom: -3px;"></div>
            * <span id="lstSml">Another browser *(for looking up errors / instructions)*.</span>
            <div style="display:block; margin-bottom: 10px;"></div>
    * Any time I save changes on a file of the <span id="songLK">**local**</span> version, the website at <span id="songTX">`http://127.0.0.1:4000/suneggv`</span> is regenerated *(unless there's some error)*. Then I just refresh the browser window to see the changes, so going from making a change to seeing it in action takes a fraction of a second.
    <div style="display:block; margin-bottom: 10px;"></div>
3. I <b id="boldWK">update</b> the <span id="birdLK">**online**</span> version once I'm satisfied with how the website's looking. On the idle terminal window from Workspace 2:[^whypull]
    - <div style="margin-top:5px;"><code class="code" id="term">$ git pull</code></div>
    - <code class="code" id="term">$ git status</code>
    - <code class="code" id="term">$ git add -A</code>
    - <code class="code" id="term">$ git commit -m "Prruuu Seeed ÔvÔ"</code>
    - <code class="code" id="term">$ git push</code>
    <div style="margin-top:5px;"></div>
4. Modifying the <span id="birdLK">**online**</span> version <b id="boldWK">generates a new CI Pipeline</b> on Gitlab, whose corresponding Jobs will be executed on the Own Runner *(aka Specific Runner)* that I made sure was working using that <code class="code" id="term">$ sudo gitlab-runner status</code> check in step 2 above *(if none of this makes sense yet, you're in the same position that I was, and I got your back[^terms])*. Gitlab knows to generate the Pipeline based on the commands in the `.gitlab-ci.yml` file of the root folder, located in the root folder of the <span id="songLK">**local**</span> version, and subsequently of the <span id="birdLK">**online**</span> version too. An easy way to check the progress of the runner is to go to the project page up on Gitlab, then <span style="white-space:nowrap">`CI/CD` &rarr; `Jobs`</span> and then go to the <span style="color: #1068BF;">`pages`</span> link of the topmost Job[^howlong].
<div style="display:block; margin-bottom: 30px;"></div>

Visually the workflow is daunting, but that's only because I'm going fairly in-depth explaining each component. In practice, the setup in step 2 takes maybe a minute, and then showing the results is almost immediate. Step 3 is also half a minute at most, depending on what you want the commit message to say. Step 4 is a bit slow, but totally hands-off, and you only need to do it occasionally. So on the part that matters, which is specifically messing around with the html and CSS of the site, the workflow is lightning-fast.

But clearly this isn't something that will work straight away. You'll need to fulfil some prerequisites, namely you'll want:

* An SSH connection to Gitlab;
* Jekyll installed locally;
* An Own Runner.

#### ***Hurdle #1*** &rarr; lorem ipsum?
I followed the documentation here

----
***Footnotes***
<div style="display:block; margin-top: 20px;"></div>

[^1]: <span class="footnote">For the record, <em>"anything I choose"</em> means birds and other uncontroversial subjects, and <em>"whenever I want"</em> means sometimes.</span>

[^2]: <span class="footnote">Yes, I've considered calling it a meta-tutorial, or a metatorial, as well y'know. I'd go ahead with it except – <em>and it's the weirdest thing</em> – I get these intense fits of nausea when writing it out, as if my body were rejecting the word because it's too embarrassing? Dunno what that's about. Anyway, threetorial it is.</span>

[^3]: <span class="footnote">I want to spend a percentage of my projected website revenue on a domain name. My projected website revenue is 0€. Also, hope the colored text is not too confusing. Those aren't links, they'd only be links if this were a post from the "<a id="birdLK" href="{{ "/birb/" | prepend: site.baseurl }}">birb</a>" or "<a id="songLK" href="{{ "/song/" | prepend: site.baseurl }}">song</a>" categories, respectively. Links for "<a id="techLK" href="{{ "/tech/" | prepend: site.baseurl }}">tech</a>" are blue.</span>

[^sudo]: <span class="footnote">I can't run those commands without <code class="code" id="term">$ sudo</code>, and <a id="techLK" href="https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3188">this thread here</a> explains that that's <em>kinda</em> by design? You may be wondering whether that's, y'know, safe. Well, turns out that if you stick to the tutorials and default runner configs, you have nothing to worry about, according to <a id="techLK" href="https://docs.gitlab.com/runner/security/">this Gitlab doc.</a> As shown here <mark>HERE</mark>, my runner is using the Docker executor, which in this case generates three containers. By default, containers run in non-privileged mode, which can be considered safe according to the Gitlab doc. But I wanted to be 100% sure that they weren't being generated in privileged mode, because the Gitlab doc tells you that's bad. My reasoning was: <em>"well, I'unno, maybe the fact that I'm using <code class="code" id="term">$ sudo</code> could change the container privileges? I dunno how to computers, man".</em> Anyway, as per <a id="techLK" href="https://stackoverflow.com/questions/32144575/how-to-know-if-a-docker-container-is-running-in-privileged-mode">this thread here</a>, I ran <code class="code" id="term">$ docker inspect --format='{% raw %}{{.HostConfig.Privileged}}{% endraw %}' &lt;container ID&gt;</code> with the three container IDs, and they all returned <code>false</code>, meaning that the runner containers are all running in non-privileged mode, and so this setup is in fact, y'know, safe.</span>

[^4]: <span class="footnote">I use the Xfce desktop environment, which allows for cycling between multiple workspaces by scrolling on an empty part of the desktop background. I prefer this to having multiple screens because it's easier to keep track of the cursor. The <em>mouse</em> cursor, yes.</span>

[^5]: <span class="footnote">This is because resizing the browser window *(e.g. to see how the site will look on mobile)* sometimes fills up the swap memory for some reason. Closing the browser and reopening it solves the problem, though.</span>

[^whypull]: <span class="footnote">It may seem a bit OCD to <code class="code" id="term">$ git pull</code> when I'm the only one working on the site, but actually it's necessary for the comment system. I explain it in <mark>PAJSFPJASPFJASPFOSA</mark></span>

[^terms]: * <span class="footnote">**CI** &rarr; Continuous Integration. ASHFIAPSFASOJFPAs </span>
    * <span class="footnote">**Pipeline** &rarr; ASFASFASFSAF </span>
    * <span class="footnote">**Job** &rarr; ASFASFSAFSAFASF</span>
    * <span class="footnote">**Runner** &rarr; ASFASFASFASFSAF</span>
    * <span class="footnote">**Own Runner** &rarr; ASFASFSAFASFAS Refer SHARED RUNNERS HERE TOO</span>
    * <span class="footnote">**Specific Runner** &rarr; Name given to Own Runners on Gitlab and pretty much nowhere from what I've seen.</span>
    * <span class="footnote">**`.gitlab-ci.yml`** &rarr; ASFIAOHSFIHSAF</span>

[^howlong]: <span class="footnote">A small thing to keep in mind: sometimes it takes a while for online version of the site to fully update, even after the pipeline has completed. It seems to occur randomly, but it's not a huge problem. If your site seems stuck in a half-updated state, just give it some time and it'll fix itself.</span>
