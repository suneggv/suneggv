---
layout: post
title:  "The Angelic Process - Weighing Souls With Sand (2007)"
date:   1500-01-21 15:32:14 -0300
categories: jekyll update
description: "Here on Niceberg 07/39, we will PRRUUU and SEEED ÔvÔ"
thumbnail: "/images-html/thumbnails/posts-music/niceberg-angelic-process.png"
trash: "no"
draft: "yes"
---
SONGOSNGONSGONG

You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a web server and auto-regenerates your site when a file is updated.

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

<!--more-->

Jekyll also offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

<a href="https://example.com">
    <button>Click me</button>
</a>

{% include /music/ankhaos-muninHugin/01synapsidBlues.html %}

Check out the {% include post-link.html link-text="Jekyll docs" link-url="http://jekyllrb.com/docs/home" %} for more info on how to get the most out of Jekyll. File all bugs/feature requests at {% include post-link.html link-text="Jekyll’s GitHub repo" link-url="https://github.com/jekyll/jekyll" %}. If you have questions, you can ask them on {% include post-link.html link-text="Jekyll Talk" link-url="https://talk.jekyllrb.com/" %}.
