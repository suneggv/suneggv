---
layout: post
title:  "The Caretaker - Everywhere at the End of Time (2019)"
date:   1500-01-14 15:32:14 -0300
categories: jekyll update
description: "Here on Niceberg 01/39, we will PRRUUU and SEEED ÔvÔ"
thumbnail: "/images-html/thumbnails/posts-music/niceberg-eateot.png"
trash: "no"
draft: "yes"
---

**This album will break you(r nose)**

The Caretaker was a project by Leyland Kirby remembered and forgotten between 2006 and 2019. Consensus says it's the oldest music ever made, so it's a fitting start. V arrived at the medium from first principles to convey a range of themes, but what did it all mean? It changed with age, with a throughline of *"memory"*. Okay, but what did it all *mean*? The Caretaker has died of old age, all we can do is speculate. But why through music? Good question, let's ask The C-- oh. As for Leyland Kirby, v's the artist, but not The Caretaker anymore: let's not pester vm.

What we have are these records of things remembered. That, and some instructions. Every Caretaker release came bundled with a broad-strokes explanation of the portrayed themes: what v wanted the waveforms to represent. But before it always felt like the explanation was competing with the listener's perception: you could arrive at another interpretation that seemed equally legitimate and much less laborious, rendering the original meaning ignorable. In effect, it was easy to come out feeling like the music was a shallow exercise in aestheticraft, because you heard it as such and chose not to engage with the themes. What's more, you'd feel justified because of *Death to the Author*: who cares what this eateot says the work's about, what does v know? I'm not the only one guilty of this: for example, interviews from the {% include post-link.html link-text="*Empty Bliss*" link-url="https://thecaretaker.bandcamp.com/album/an-empty-bliss-beyond-this-world" %} era would often brush aside its theme in favor of the conveyed mood (<mark>ADD EXAMPLES</mark>).

I think the issue was that the older releases represented a snapshot in time, a relatively static condition, and carnivores see things better when they move. Anyway, that's before. This time, good luck *not* engaging with the themes. {% include post-link.html link-text="*Everywhere at the End of Time*" link-url="https://thecaretaker.bandcamp.com/album/everywhere-at-the-end-of-time" %} shoves your face right into what it's about with such force that you'll be using the explanation to soften the blow. Then someone asks what's with your nose, and you'll go *"well, there's this one album about dementia.."*.


Eateot is constructed from first principles, which makes it very intuitive once you've understood it,


This album successfully evokes the feeling of *"My health problems make me a burden to my family and it's selfish to continue bothering them"*. I didn't even know that's a specific feeling to evoke, but apparently it's track F6, *"An empty bliss beyond this world"*. Listen to it and tell me that's not the sound of lying sick in bed on New Year's Eve, hearing your friends celebrate on the other side of the door and knowing that, even though they love you, they're relieved you're not there. Fine, to each their own, then.





WHAT I WANT TO TALK ABOUT:

* This album is actually well crafted
* Artist's intention vs audience
* Minor worldbuilding
* The specific imagery doesn't matter
* If you want to dissect the album, make sure that you don't allow that to muddle your perception of your own experience with it
