---
layout: post
title:  "Is the Samsung Galaxy A22 e-waste?"
date:   1500-02-22 15:32:14 -0300
categories: jekyll update
description: "Test post for the tech category"
thumbnail: "/images-html/thumbnails/posts-tech/samsung-a22-ewaste.png"
trash: "no"
draft: "yes"
---
Thumbnail says it all, but why ask? On the {% include post-link.html link-text="GSMArena review" link-url="https://www.gsmarena.com/samsung_galaxy_a22-review-2357.php" %} of the A22, user `unxpctd` posted the following comment[^unxpctd]:
<div style="display:block; margin-top: -13px;"></div>
> I know this is a low-end phone and all, but I really dislike phones like this. I call them e-waste because that's exactly what they are. We don't have unlimited resources on this planet, but phones like this just ignore that fact when their materials could be used to make something better or more full featured.

<div style="display:block; margin-top: -13px;"></div>
... the implication being *(as far as I understand)* that Samsung committed a moral nono by producing this low-end[^lowend] phone, and that if you buy it then ~~you~~ I committed a moral nono too. Darn. I think issues of this kind -- *on the moral nono-ity or yesyes-ness of different decisions* -- will be increasingly pressing as we figure out a new moral *"protocol"* for the next few centuries, so let's look at this case more in-depth.

<!--more-->

<div style="display:block; margin-top: 42px;"></div>
`Unxpctd`'s reason why it's a nono is encoded in the phrase: *"their materials could be used to make something better or more full featured"*. Seems fairly straightforward, but not so fast: it can mean two things. **`Alternative 1`** is that it means:
<div style="display:block; margin-top: -13px;"></div>
> The materials used in the construction of the produced instances of this phone model could have been applied to constructing instances of a more top-of-the-line phone model.

<div style="display:block; margin-top: -13px;"></div>
*I.e. it's e-waste because it's a low-end[^lowend] phone*. **`Alternative 2`** is that it means:
<div style="display:block; margin-top: -13px;"></div>
> The materials used in the construction of the produced instances of this phone model could have been applied to constructing instances of better phones, top-of-the-line or not.

<div style="display:block; margin-top: -10px;"></div>
*I.e. it's e-waste because it's not a competitive offer*.

<div style="display:block; margin-top: 42px;"></div>

#### ***Alternative 1 -- e-waste bc low-end[^lowend]***

For this to be true, we need to accept that buying higher-end phones is at least *likely*[^whyprobably] less wasteful than buying low-end[^lowend] phones. I see two ways for this to be true, that I list below:

* ***`Way 1`***: positive correlation between high-end-ness and more eco-friendly construction practices;
* ***`Way 2`***: superior longevity.

#### Alt.1, Way 1

It's reasonable to suppose that a company focusing on higher-end phones will have better means to ensure eco-friendly practices than one focusing on low-end phones: if money isn't an issue, then efficiency no longer needs to be the main criterion for selecting construction methods, so at least there's more flexibility there. If we were talking about the Peepee luxury phone company vs. Poopoo's shartphones, okay, sure, makes sense. But the line's a lot blurrier here: Samsung makes phones across a wide spectrum. We *could* try to assume that their higher-end phones are constructed in a more eco-friendly manner than their lower-end phones, but I doubt that, one, a correlation would be present, and B, that it would go unadvertised.

#### ***Alternative 2 -- e-waste bc not competitive***

This

----
***Footnotes***
<div style="display:block; margin-top: 20px;"></div>

[^unxpctd]: <span class="footnote">See "***References***" for screencap.</span>

[^lowend]: <span class="footnote">Actually it's more like a mid-range phone, in the sense that in reviews it's consistently referred to as such, and it competes with other phones that are consistently referred to as mid-range as well. You could argue that even the phones it's competing with are low-end because *everything* at the price-point is low-end due to weak specs: that's legitimate way of looking at things... Really it depends on what purpose you consider terms like *"low-end"* and *"mid-range"* to primarily serve: is it to help one choose a model within a certain price-range; or is it more for establishing a hierarchy of phone models? If it's the former *(which I think makes more sense in this case)*, then I say let it be called a duck if it insists on quacking. Maybe what was meant was that the A22 is low-end, and the others in its price-range are mid-range. You *could* claim that, yes, but if you mean it as an insult, it doesn't work: you're just accidentally implying that the phone knows its niche.</span>

[^whyprobably]: <span class="footnote">I'm saying *"likely"* here because this is all based on predictions of what should happen. *Obviously* it's not a physical impossibility that a company would destroy a rainforest for every unit constructed of a flagship phone model, or that another would turn an endangered species into a least concern one every time they pump out a unit of their cheapo slab, even though both are clearly absurd scenarios. The *"likely"* is there just so as to not fall for the trap of going on a frequentist case-by-case basis, which goes against the spirit of the original comment. Instead, we're saying that *"if you buy a higher-end phone, then there's a >50% likelihood that you have been less wasteful than if you had bought a lower-end phone and otherwise acted exactly as you would have normally done"*.</span>
