---
layout: page
title: "Thanks for commenting ÔvÔ"
permalink: /thx4comm/
---

Your comment will take a few mins-hrs to show up, but it hasn't disappeared into the ether, no need to worry there. This is a static website *(html+css only)*, so posting comments requires a bit of a workaround: the comment is sent to a {% include post-link.html link-text="staticman" link-url="https://staticman.net" %} instance, which in turn sends me a merge request, which I then have to approve. The whole thing takes a bit of time, but I'm sure I'll approve the comment once I see it. In fact, most likely I'll be really happy about what you said.

![horatio]({{ "/images-md/other/horatio.jpg" | prepend:site.baseurl }} "horatio"){:class="img-responsive" style="width:50%; display:block; margin-left:auto; margin-right:auto; margin-top: 30px; margin-bottom: 30px;"}

{% comment %}
*PS: e-mails appear to me as a jumble of characters. I have no idea what your e-mail is, don't worry.*
{% endcomment %}
