---
layout: page
title: "Pigeons below ate that link :c"
permalink: /noemailhere/
---

The mailto / socials links are being ground into crop milk right now. The pigeons also ate some of the characters, so I had to replace them for similar-looking ones. For example, they took a special liking to the latin vees, so I had to replace all of them for greek nyoos.

![pidges]({{ "/images-md/other/pidges_small.jpg" | prepend:site.baseurl }} "pidges"){:class="img-responsive" style="width:75%; display:block; margin-left:auto; margin-right:auto; margin-top: 50px;"}
