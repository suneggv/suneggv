---
layout: page
title: "'bout"
permalink: /bout/
---

This just a release valve for convictions and projects. Subjects are listed above, with `birb` most likely to focus on song and city birds, `song` on bdm / tdm, and `tech` on data sci and astro stuff.

Website is static: {% include post-link.html link-text="jekyll" link-url="https://jekyllrb.com" %} *(`minima` base theme)*, html and CSS. No JS or PHP. Comments handled by {% include post-link.html link-text="staticman," link-url="https://staticman.net" %} spam protection a simple honeypot for now.

I'm also a musician from Portugal. You can find my stuff at the usual places if you look for it.

<p style="text-align: center; margin-top:50px; margin-bottom: -20px;"><em>Hope you enjoy your stay <img style="margin-top:5px;" src="{{ "/images-html/favicon/favicon-57.png" | prepend:site.baseurl }}" alt="img failed to load :c" height="35"></em></p>
